# Workflow Manager - Proposition, Requirements and Initial Steps

## Introduction

### Synchronous Sprint Planning

Initially, [OpenCraft][opencraft] was following a synchronous sprint planning process which required a cell[^opencraft-cell] wide fortnightly meeting to distribute the required work and make sure no one is tackling more than they can handle.[^sync-sprint-planning-process]

The synchronous sprint planning process was affecting members in distant timezones, such as UTC +-6 or more, which started a discussion to rethink the meeting times.[^rethinking-meeting-times] In addition, a new discussion was created to discuss the possibility of shortening the meetings by moving some of its agenda asynchronously.[^shorter-meetings]

### Asynchronous Sprint Planning

After a couple rounds of back and forth discussions, an asynchronous sprint planning process was proposed.[^async-sprint-planning-proposal] The proposed process involved distributing the work that was done during the fortnightly meeting across the entirety of the sprint; members would be responsible of a planning checklist with time-framed tasks to help plan the sprint. After testing the process in one of the OpenCraft cells, the synchronous meetings were replaced with the asynchronous sprint planning process.[^async-sprint-planning-process]

### Asynchronous Sprint Planning Checklists

#### Google Docs Checklist

The first sprint planning checklist started out as a Google Docs checklist.[^google-docs-sprint-planning-checklist] The checklist, back then, consisted of a table, for each member, containing the task description, associated role, and task status. Although this was a good start, there were still some points that needed improvement, which mainly were:

* Gaining a quicker overview of the whole cell's progress.<br>
In order to gain an overview of the whole cell's progress, it is required that each Google Doc checklist, for each member, is opened and scrolled through, which was inconvenient.
* Providing a friendlier experience for updating a task's status.<br>
In order to update a task's status, you'd need to copy a check-mark (✔️) and paste it into each row's task status column, which was also inconvenient.

#### Google Sheets Checklist

Accordingly, the sprint planning checklist was moved to Google Sheets where a single checklist would be created for the whole cell.[^google-sheets-sprint-planning-checklist] This facilitated checking the whole cell's progress. In addition, it provided the cell members with a friendly experience when it came to updating or viewing each task's status.

The Google Sheets provided the necessary user experience for both members and managers, some automation could be done to some of the tasks. Although that would be possible using Google Sheets, it presents a lot of difficulties that would be resolved through the use of the other solutions.

#### Monday.com Checklist

After some investigation,[^sprint-planning-checklist-suitable-solution] the next chosen solution was monday.com.[^mondaycom-sprint-planning-checklist] The reason behind that choice is because it provides a better experience than the other proposed solutions, on different levels:

* Checklist user/Cell member
* Checklist reviewer/Cell managers
* Automation

Although monday.com is a great software solution for the sprint planning checklist; however, it remains a proprietary software. Since OpenCraft values open source, an open source software solution needs to be created because the available ones are not aligned with the current needs.[^sprint-planning-checklist-suitable-solution]

## Problem

As mentioned earlier, the tools that are most aligned with OpenCraft's checklist needs are the proprietary software:

* [Process Street][process-st]
* [Monday.com][monday-com]

Therefore, it's required that OpenCraft creates the open source software needed because the ones currently available, both, do not satisfy the current checklist needs and might not be heading in the same direction.

### Open Source Software Solution Reqiurements

OpenCraft defines a set of requirements to guarantee a strong open source solutions, which are:

* [ ] Dedicated official project site - includes presentation of the project, features description, etc. Directs user to create an account on our SaaS offer or to documentation install it themselves
* [ ] Reliable one-liner installation without prerequisites and sensible defaults (eg. like discourse)
* [ ] Reliable one-line/click release upgrades & packaging (again, like discourse, or tutor)
* [ ] Good documentation
* [ ] All discussions are held in public (including planning & task tracking): dedicated discourse forum & gitlab bugtracker
* [ ] Extensible/pluggable architecture with a minimal core (like Wordpress, Tutor):
* [ ] Keeping OpenCraft-specific theming & logic in separate add-ons (including SaaS payment/subscription flow, to allow stand-alone installs)
* [ ] Allowing users to extend it too (safely without requiring our review as much as possible, including in our SaaS hosting offer, though this doesn’t need to be an available option for SaaS users from the start, just something that the architecture needs to allow safely)
* [ ] Open source community outreach:
* [ ] List the project in open source directories (eg alternativeto.net)
* [ ] Ask other open source projects for help testing/specifying (host them for free, target adoption for feedback & contributions)
* [ ] Fully AGPL source code

### SaaS Service Requirements

* [ ] The instance we use for ourselves should be public facing, and allow other orgs/users to use it too (multi-tenant shared hosting that doesn't require one VM/container per site, to keep hosting costs low)
* [ ] Public registration & onboarding flow
* [ ] Includes paid component (billing can be manual at first)

### UX-first Approach Requirements

* [ ] All UX designed by Ali/Cassie (mockups and flow before any development is scheduled for a sprint)
* [ ] UX review & test of all changes part of code review before any merge
* [ ] Small development iterations (1 release per sprint, from the very first sprint)
* [ ] Systematic user testing & feedback analysis (internal & external users) of all new features
* [ ] Product management: Ali/Cassie, reviewed by Xavier

### OpenCraft Checklist Solution Requirements

* [ ] Be web-based and supports team collaboration
* [ ] Support central checklist templates
* [ ] Support recurring checklists
* [ ] Filter views automatically by role/account
* [ ] Send follow-up emails / reminders for upcoming & missed items
* [ ] Offer tools to automate certain steps and checks from the checklist

## Proposal: Workflow Manager

### Terminology

Before defining the requirements, it is important to define certain terms which will be used frequently later:

* **Task**: A task is an action which needs to be completed by the user. A task can have multiple attributes, such as: summary, description, deadline, etc...
* **Conditional Task**: A conditional task is an action which only is applicable when a certain condition is true. For example, tasks that are specific to a role are conditional tasks because they shouldn't be visible for other members who do not share that role.
* **Workflow**: A workflow is a template formed of a set of ordered tasks, of different kinds.
* **Workflow run**: A workflow run is an instance of a specific workflow. A workflow run contains a set of ordered tasks. Workflow runs have a specific start and end date and assignee(s).
* **Task Tracker**: A task tracker is a system which manages and maintains tasks. The task tracker is aware of a task's different attributes, such as status, assignee(s), dependencies or blockers, and the transition between statuses.
* **Workflow Manager**: A workflow manager is a system which manages and maintains workflow and workflow runs. The workflow manager has a complete description of the process of the workflow. It is also aware of the order of completion of the tasks in a workflow run, and their details.
* **View**: A view is a way of displaying tasks in a workflow or workflow run.
* **Documentation View**: A documentation view refers to a view which showcases tasks integrated directly into some documentation.
* **Table View**: A table view refers to a view which showcases tasks and each's attributes in a table.

### Requirements

The following are some key requirements for the workflow manager.

* [ ] **Tasks**<br>
Tasks are the basic building build for any particular process or workflow; without specific actions that need to be taken, there are no processes.
* [ ] **Conditional Tasks**<br>
Conditional tasks provide a way to utilize the same workflow for different groups or roles. For example, a single workflow might contain the tasks of all groups or roles. Yet, the workflow run would then only showcase the information that is related to the specific user.
* [ ] **Workflows**<br>
Workflows define a process with a set of tasks, which can be used many times without having to do the busy work of redefining it everytime it needs to be used.
* [ ] **Workflow runs**<br>
Workflow runs are the actual process which the assignee(s) are responsible for finishing during a particular period of time.
* [ ] **Workflow run and task start and end dates**<br>
Start and end dates help identify the timeframe to complete a certain task or workflow run. They also help identify whether the assignee(s) are behind on their tasks. Not all workflow runs or tasks might have a deadline.
* [ ] **Table view**<br>
The table view provides a good overview of the different tasks available in a workflow and a workflow run, as well as the attributes of each task.
* [ ] **Documentation view**<br>
The documentation view provides a good guide which would be easy for members to follow, because it would contain information about the task and any other information that can help accomplish a task.
* [ ] **Workflow run and task comments**<br>
Commenting on workflow runs and tasks provides the opportunity to ping or discuss any unexpected updates.
* [ ] **Progress tracking tools**<br>
Progress tracking tools provide great way to have an overview about the progress of each assignee in their process. This helps give an overview of what's been done, what's left to do, and any communication with the assignee that might be required.
* [ ] **Developer tools**<br>
Developer tools allow third party software to create, update, edit, and delete tasks, workflows, and workflow runs. In addition, they provide third party software with information regarding when a task status is updated, a workflow is completed, a task is late, or a workflow is late, etc...

### Next Steps

The next steps, after agreeing on the workflow manager proposal, would be to create a UX/UI discovery to identify the deliverables that are needed in order to start the development process.

#### UX/UI Discovery Scope

* Evaluate the brief, help to refine the scope/requirements, define a suggested approach & plan for the UX work that will be necessary to get to the finalized mockups, and an estimate of the monthly UX time commitment needed to complete the UX work being planned.

* As much as possible, the UX work should be planned in stages, to allow the development to happen iteratively:
  * Each UX task should target preparing work sized to a single sprint of the developers (basic proof of concept at first, designing and implementing a small portion of a feature each time, etc.);
  * Plan to schedule further UX tasks iteratively - while developers work on the outcome of the first UX task, work on the second UX task to prepare the developers’ next sprint, and so on.
  * Developers (& Xavier Antoviaque) are reviewers on UX tasks, and Ali/Cassie are 2nd reviewers on developer tasks - this way developers can provide feedback on what can be achieved during their next sprint. Ali/Cassie write the stories for the developers as part of sprint planning, and the epic owner refine/split them as needed to take into account technical constraints.
  * UX time requirement estimates are determined (as hours/month), and availability from Ali/Cassie is checked, and discussed as part of the epic planning management. When sprint planning managers prepare their cell availability, they also check with Ali Hugo the amount of UX hours the cell will need to progress on its epics. during the initial discovery of each epic.

* The work planned for the first development sprint should include a minimal MVP/prototype usable at the end of the sprint, based on one of the existing workflows from OpenCraft (to be decided - it could be one of the simpler ones).

## Ideas/Brainstorming

* Documentation views will be combining documentations and workflows. In order to do that, we can extend the markdown syntax to refer to certain workflow configurations and ids similarly to how footnotes would work.
* Documentation views already would contain documentations and workflows. Therefore, it could be useful to provide hosting for docs so that other services using markdown docs would be able to switch to our tool and utilize and pay for workflows.
* Version tracking can be provided by utilizing git to version and deploy the changes. WYSIWYG inline editting interfaces would help non-technical people utilize the tool as well, while still utilizing git for versioning and deploying changes. An example to this would be NetlifyCMS.

<!-- FOOTNOTES -->

[^opencraft-cell]: [*Cells*][cells-handbook-section] - [Handbook][handbook] section.
[^sync-sprint-planning-process]: [*Sprint Planning Agenda*][sync-sprint-planning-agenda-handbook-section] - Synchronous Process - [Handbook][handbook] section.
[^rethinking-meeting-times]: [*Rethinking Meeting Times*][rethinking-meeting-times-forum-thread] - [Forum][forum] thread.
[^shorter-meetings]: [*Shorter Meetings*][shorter-meetings-forum-thread] - [Forum][forum] thread.
[^async-sprint-planning-proposal]: [Asynchronous Sprint Planning Proposal][async-sprint-planning-proposal-pull-request].
[^async-sprint-planning-process]: [*Sprint Planning Agenda*][async-sprint-planning-agenda-handbook-section] - Asynchronous Process - [Handbook][handbook] section.
[^google-docs-sprint-planning-checklist]: [Google Docs Sprint Planning Checklist][google-docs-sprint-planning-checklist-template].
[^google-sheets-sprint-planning-checklist]: [Google Sheets Sprint Planning Checklist][google-sheets-sprint-planning-checklist-template].
[^sprint-planning-checklist-suitable-solution]: Sprint Planning Checklist - [Suitable Software Solution Discovery][sprint-planning-checklist-suitable-solution-discovery].
[^mondaycom-sprint-planning-checklist]: [Monday.com Sprint Planning Checklist][mondaycom-sprint-planning-checklist-template].

<!-- LINKS -->

[opencraft]: https://opencraft.com/ "OpenCraft"
[handbook]: https://handbook.opencraft.com/en/latest "OpenCraft Handbook"
[forum]: https://forum.opencraft.com/ "OpenCraft Forum"
[cells-handbook-section]: https://handbook.opencraft.com/en/latest/cells/ "OpenCraft Cells"
[sync-sprint-planning-agenda-handbook-section]: https://gitlab.com/opencraft/documentation/public/-/blob/d8f2f663bfb6cc22e6014dd433f5979a5157f051/handbook/sprint_planning_agenda.md "Sprint Planning Agenda - Synchronous Process"
[async-sprint-planning-agenda-handbook-section]: https://gitlab.com/opencraft/documentation/public/-/blob/46be170d4995ddc4d9de71ed7b15895ed6d3f31c/handbook/sprint_planning_agenda.md "Sprint Planning Agenda - Asynchronous Process"
[rethinking-meeting-times-forum-thread]: https://forum.opencraft.com/t/rethinking-meeting-times/584?u=nizar "Rethinking Meeting Times Forum Thread"
[shorter-meetings-forum-thread]: https://forum.opencraft.com/t/shorter-meetings/585?u=nizar "Shorter Meetings Forum Thread"
[async-sprint-planning-proposal-pull-request]: https://gitlab.com/opencraft/documentation/public/-/merge_requests/173 "Asynchronous Sprint Planning Proposal"
[google-docs-sprint-planning-checklist-template]: https://docs.google.com/document/d/1MshgW5-uMwgpFfgWoDEYZSraZrpi8QmZO0XnH1A-npA/edit?usp=sharing "Google Docs Sprint Planning Checklist"
[google-sheets-sprint-planning-checklist-template]: https://docs.google.com/spreadsheets/d/1RctTurFriLHKdVlPLlIuqr71UU7GQ8ssM6exeMh4Jfs/edit?usp=sharing "Google Sheets Sprint Planning Checklist"
[sprint-planning-checklist-suitable-solution-discovery]: https://docs.google.com/document/d/1UpSM6-Tk9V0tUvMnV0lbySu9fAIGwsfwR3HYs_hUr0I/edit#heading=h.j8blnxcjqp14 "Sprint Planning Checklist Software Solution Discovery"
[mondaycom-sprint-planning-checklist-template]: https://view.monday.com/942397140-b2d0d779a236cf486837f8c7391a08d9 "Monday.com Sprint Planning Checklist"
[monday-com]: https://monday.com/ "Monday.com"
[process-st]: https://process.st/ "Process Street"
[read-the-docs]: https://readthedocs.org/ "ReadTheDocs"
